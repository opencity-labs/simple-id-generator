# For more information, please refer to https://aka.ms/vscode-docker-python
FROM python:3.12-slim

ARG USERNAME=app
ARG USER_GID=1000
ARG USER_UID=$USER_GID

# Create the user
RUN groupadd --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE=1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1

ENV TZ=Europe/Rome

# Install pip requirements
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY /src /app/src

RUN chown -R $USERNAME:$USERNAME /app
RUN chmod +x /app/src/*.py

USER $USERNAME

WORKDIR /app/src

CMD [ "python3", "app.py" ]
