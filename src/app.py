from contextlib import asynccontextmanager

import uvicorn
import os
from fastapi import FastAPI, HTTPException
from dotenv import load_dotenv
from models import ApplicationsByServiceCounter, SDCClient, IdModel, ServiceNotFoundError
from __init__ import __version__ as app_version
from logger import app_logger as logger

load_dotenv()

logger.info('Starting')
APP_HOST = os.environ.get('APP_HOST', '0.0.0.0')
APP_PORT = os.environ.get('APP_PORT', 8000)
SDC_BASE_URL = os.environ.get('SDC_BASE_URL')
SDC_ADMIN_USERNAME = os.environ.get('SDC_ADMIN_USERNAME')
SDC_ADMIN_PASSWORD = os.environ.get('SDC_ADMIN_PASSWORD')
SDC_SERVICE_SLUGS = os.environ.get('SDC_SERVICE_SLUGS', 'segnalazione-interna-disservizio:G')
SDC_SERVICE_IDENTIFIERS = os.environ.get('SDC_SERVICE_IDENTIFIERS', 'inefficiencies:S')
ID_OFFSET_COUNT = os.environ.get('ID_OFFSET_COUNT', 1000)


@asynccontextmanager
async def manage_startup(app: FastAPI):
    logger.info('Initializing API ...')
    try:
        init_counters()
    except Exception as e:
        logger.error(f'An error occurred while getting initial count: {e}')
        raise

    yield

app = FastAPI(title='Simple ID Generator', version=app_version, lifespan=manage_startup)

applications_count = []


def init_counters():
    global applications_count
    try:
        sdc_client = SDCClient(base_url=SDC_BASE_URL, username=SDC_ADMIN_USERNAME, password=SDC_ADMIN_PASSWORD)
        for identifier_and_prefix in SDC_SERVICE_IDENTIFIERS.split(','):
            splitted = identifier_and_prefix.split(':', 2)
            identifier = splitted[0]
            prefix = splitted[1]
            logger.info(f'Service conf identifier={identifier} prefix={prefix}')
            applications_count.append(
                ApplicationsByServiceCounter(
                    service=identifier,
                    count=(sdc_client.get_last_id(identifier) + int(ID_OFFSET_COUNT)),
                    prefix=prefix
                )
            )
            logger.info(
                f"Service with identifier {identifier}: "
                f"starting to count from {applications_count[-1].count}"
            )
        for slug_and_prefix in SDC_SERVICE_SLUGS.split(','):
            splitted = slug_and_prefix.split(':', 2)
            slug = splitted[0]
            prefix = splitted[1]
            logger.info(f'Service conf slug={slug} prefix={prefix}')
            applications_count.append(
                ApplicationsByServiceCounter(
                    service=slug,
                    count=(sdc_client.get_last_id(slug, field='service') + int(ID_OFFSET_COUNT)),
                    prefix=prefix
                )
            )
            logger.info(
                f"Service with slug {slug}: "
                f"starting to count from {applications_count[-1].count}"
            )
    except Exception as e:
        logger.error(f'An error occurred while setting json with applications count: {e}')
        raise


def generate_id(service):
    for item in applications_count:
        if item.service == service:
            item.count += 1
            return IdModel(id=(item.prefix + str(item.count)), service=service)


@app.get('/id/byidentifier/{service_identifier}', response_model=IdModel)
def generate_simple_id_by_service_identifier(service_identifier):
    try:
        generated_id = generate_id(service_identifier)
        if generated_id:
            return generated_id
        raise ServiceNotFoundError(f'Service identifier {service_identifier} not found')
    except ServiceNotFoundError as e:
        logger.error(f'An error occurred: {e}')
        raise HTTPException(status_code=404)
    except Exception as e:
        logger.error(f'An error occurred while generating simple id: {e}')
        raise HTTPException(status_code=500)


@app.get('/id/byslug/{service_slug}', response_model=IdModel)
def generate_simple_id_by_service_slug(service_slug):
    try:
        generated_id = generate_id(service_slug)
        if generated_id:
            return generated_id
        raise ServiceNotFoundError(f'Service slug {service_slug} not found')
    except ServiceNotFoundError as e:
        logger.error(f'An error occurred: {e}')
        raise HTTPException(status_code=404)
    except Exception as e:
        logger.error(f'An error occurred while generating simple id: {e}')
        raise HTTPException(status_code=500)


if __name__ == '__main__':
    uvicorn.run(
        app,
        host=APP_HOST,
        port=APP_PORT,
        proxy_headers=True,
        forwarded_allow_ips='*',
        access_log=False,
    )

