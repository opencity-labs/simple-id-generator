import unittest
from unittest.mock import patch, MagicMock
from fastapi.testclient import TestClient
from app import app, generate_id, init_counters, applications_count
from models import IdModel, ApplicationsByServiceCounter, SDCClient
from dotenv import load_dotenv

load_dotenv()


class TestApp(unittest.TestCase):
    def setUp(self):
        self.client = TestClient(app)

    @patch('app.SDCClient')
    def test_init_counters(self, MockSDCClient):
        mock_client = MockSDCClient.return_value
        mock_client.get_last_id.return_value = 123

        init_counters()

        self.assertGreater(len(applications_count), 0)
        self.assertIsInstance(applications_count[0], ApplicationsByServiceCounter)
        self.assertEqual(applications_count[0].count, 101)

    def test_generate_id(self):
        applications_count.append(ApplicationsByServiceCounter(service='test_service', count=100, prefix='TS-'))
        generated_id = generate_id('test_service')
        self.assertEqual(generated_id.id, 'TS-101')
        self.assertEqual(generated_id.service, 'test_service')

    @patch('app.generate_id')
    def test_generate_simple_id_by_service_identifier(self, mock_generate_id):
        mock_generate_id.return_value = IdModel(id='TS-101', service='test_service')
        response = self.client.get('/id/byidentifier/test_service')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {"id": "TS-101", "service": "test_service"})

    @patch('app.generate_id', return_value=None)
    def test_generate_simple_id_by_service_identifier_not_found(self, mock_generate_id):
        response = self.client.get('/id/byidentifier/unknown_service')
        self.assertEqual(response.status_code, 404)

    @patch('app.generate_id')
    def test_generate_simple_id_by_service_slug(self, mock_generate_id):
        mock_generate_id.return_value = IdModel(id='TS-102', service='test_slug')
        response = self.client.get('/id/byslug/test_slug')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {"id": "TS-102", "service": "test_slug"})

    @patch('app.generate_id', return_value=None)
    def test_generate_simple_id_by_service_slug_not_found(self, mock_generate_id):
        response = self.client.get('/id/byslug/unknown_slug')
        self.assertEqual(response.status_code, 404)


class TestSDCClient(unittest.TestCase):

    def setUp(self):
        self.client = SDCClient(base_url='http://fakeapi.com', username='user', password='pass')

    @patch('models.requests.post')
    def test_get_token(self, mock_post):
        mock_post.return_value.json.return_value = {'token': 'fake_token'}
        token = self.client._token
        self.assertEqual(token, 'fake_token')

    @patch('models.requests.get')
    @patch.object(SDCClient, '_token', new_callable=MagicMock, return_value='fake_token')
    def test_get_applications_count(self, mock_token, mock_get):
        mock_get.return_value.json.return_value = {'meta': {'count': 10}}
        count = self.client.get_applications_count(service_id='service1')
        self.assertEqual(count, 10)

    @patch('models.requests.get')
    @patch.object(SDCClient, '_token', new_callable=MagicMock, return_value='fake_token')
    def test_get_last_id(self, mock_token, mock_get):
        mock_get.return_value.json.return_value = {
            'data': [{'id': 'app123', 'data': {'subject': 'S45 - Some other data'}}],
            'meta': {'count': 10}
        }
        last_id = self.client.get_last_id(service_id='service1')
        self.assertEqual(last_id, 45)


if __name__ == '__main__':
    unittest.main()
