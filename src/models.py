import requests
import re
from functools import cached_property
from pydantic import BaseModel
from logger import models_logger as logger


class ServiceNotFoundError(Exception):
    pass


class IdModel(BaseModel):
    id: str
    service: str


class ApplicationsByServiceCounter(BaseModel):
    service: str
    count: int
    prefix: str


class SDCClient:
    def __init__(self, base_url, username, password):
        self.base_url = base_url
        self.username = username
        self.password = password

    @cached_property
    def _token(self):
        try:
            logger.info('Getting authentication token')
            response = requests.post(
                f'{self.base_url}/auth',
                json={'username': self.username, 'password': self.password}
            )
            response = response.json()
            logger.debug(response)
            return response['token']
        except Exception as e:
            logger.error(f'An error occurred while generating the auth token: {e}')

    def get_applications_count(self, service_id, field='service_identifier'):
        try:
            response = requests.get(
                url=f'{self.base_url}/applications?{field}={service_id}&limit=1',
                headers={'Accept': 'application/json', 'Authorization': f'Bearer {self._token}'}
            )
            response = response.json()
            logger.debug(response)
            return response['meta']['count']
        except Exception as e:
            logger.error(f'An error occurred while getting applications count from API: {e}')

    def get_last_id(self, service_id, field='service_identifier'):
        try:
            response = requests.get(
                url=f'{self.base_url}/applications?{field}={service_id}&sort=DESC&limit=10',
                headers={'Accept': 'application/json', 'Authorization': f'Bearer {self._token}'}
            )
            response = response.json()
            logger.debug(response)
            for application in response['data']:
                application_id = application['id']
                subject = application['data']['subject']
                sequential_id = ''
                m = re.search(r'^\s*[a-zA-Z]?(\d+)\s*-.*', subject)
                if m is not None:
                    sequential_id = m.group(1)
                if sequential_id.isnumeric():
                    logger.info(
                        f'Getting last used id for service {field}={service_id}: application="{application_id}" '
                        f'subject="{subject}" sequential_id="{sequential_id}"'
                    )
                    return int(sequential_id)
            return response['meta']['count']

        except Exception as e:
            logger.error(f'An error occurred while getting applications count from API: {e}')
